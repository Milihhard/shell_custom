![Mesh](res/Mesh.svg)

# MESH (Modular and Extended SHell) #

MESH is a Shell based on bash.

The aim is to improves shell visibility by adding modules on it.

The idea of this extended shell came from the hassle to chain `cd` and `ls` command when I wanted to explore my projects.

> Includes a todo command.

![screenshot](res/screenshot.png)

From top left to right bottom:
1. Older commands
1. Directory (ls command)
1. Todo
1. Last output command
1. WIP: async output command

